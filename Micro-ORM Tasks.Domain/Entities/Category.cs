﻿using System.Collections.Generic;

namespace Micro_ORM_Tasks.Domain.Entities
{
	public class Category
	{
		public int CategoryID { get; set; }
		public string CategoryName { get; set; }
		public string Description { get; set; }

		//TODO:
		// public TYPE Picture { get; set; }
		public IList<Product> Products { get; set; }
	}
}
