﻿using System.Collections.Generic;

namespace Micro_ORM_Tasks.Domain.Entities
{
	public class Product
	{
		public int ProductID { get; set; }
		public string ProductName { get; set; }
		public int SupplierID { get; set; }
		public int CategoryID { get; set; }
		public IList<OrderDetail> OrderDetails { get; set; }
	}
}