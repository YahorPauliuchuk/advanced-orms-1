﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Micro_ORM_Tasks.WebUI.Services;
using Micro_ORM_Tasks.Domain.Entities;
using Micro_ORM_Tasks.DAL.Components;

namespace Micro_ORM_Tasks.WebUI.Controllers
{
	public class CategoryController : ApiController
	{
		private CategoryService categoryService;

		public CategoryController()
		{
			CategoryRepo categoryRepo = new CategoryRepo();
			categoryService = new CategoryService(categoryRepo);
		}

		public CategoryController(CategoryService categoryService)
		{
			this.categoryService = categoryService;
		}

		[Route("Categories")]
		[HttpGet]
		public IEnumerable<Category> GetCategories()
		{
			return categoryService.GetAllCategories();
		}

		[Route("Categories/{id}")]
		[HttpGet]
		public Category GetCategory(int id)
		{
			return categoryService.GetCategoryById(id);
		}
		// TODO:
		[Route("Category")]
		[HttpPost]
		public bool Post([FromBody]Category category)
		{
			throw new NotImplementedException();
		}
		// TODO:
		[Route("Category")]
		[HttpPut]
		public bool Put([FromBody]Category category)
		{
			throw new NotImplementedException();
		}
	}
}