﻿using System.Collections.Generic;
using System.Web.Http;
using Micro_ORM_Tasks.WebUI.Services;
using Micro_ORM_Tasks.Domain.Entities;
using Micro_ORM_Tasks.DAL.Components;

namespace Micro_ORM_Tasks.WebUI.Controllers
{
	public class OrderController : ApiController
	{
		private OrderService orderService;

		public OrderController()
		{
			OrderRepo orderRepo = new OrderRepo();
			orderService = new OrderService(orderRepo);
		}

		[Route("Orders")]
		[HttpGet]
		public IEnumerable<Order> GetAllOrders()
		{
			return orderService.GetAllOrders();
		}
	}
}