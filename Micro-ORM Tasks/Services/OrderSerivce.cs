﻿using System.Collections.Generic;
using Micro_ORM_Tasks.DAL.Interfaces;
using Micro_ORM_Tasks.Domain.Entities;

namespace Micro_ORM_Tasks.WebUI.Services
{
	public class OrderService
	{
		private IOrderRepo orderRepo;

		public OrderService(IOrderRepo orderRepo)
		{
			this.orderRepo = orderRepo;
		}

		public IEnumerable<Order> GetAllOrders()
		{
			return orderRepo.GetAllOrders();
		}

		public Order GetOrderById(int id)
		{
			return orderRepo.GetOrderById(id);
		}

		//TODO
	}
}