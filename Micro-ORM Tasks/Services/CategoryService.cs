﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Micro_ORM_Tasks.DAL.Interfaces;
using Micro_ORM_Tasks.Domain.Entities;

namespace Micro_ORM_Tasks.WebUI.Services
{
	public class CategoryService
	{
		private ICategoryRepo categoryRepo;

		public CategoryService(ICategoryRepo categoryRepo)
		{
			this.categoryRepo = categoryRepo;
		}

		public IEnumerable<Category> GetAllCategories()
		{
			return categoryRepo.GetAllCategories();
		}

		public Category GetCategoryById(int id)
		{
			return categoryRepo.GetCategoryById(id);
		}

		//TODO:
		//public bool InsertCategory(Category category)
	}
}