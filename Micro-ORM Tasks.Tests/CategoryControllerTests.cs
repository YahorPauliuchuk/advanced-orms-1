﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Micro_ORM_Tasks.WebUI.Services;
using Micro_ORM_Tasks.WebUI.Controllers;
using Micro_ORM_Tasks.DAL.Interfaces;
using Micro_ORM_Tasks.DAL.Components;
using Micro_ORM_Tasks.Domain.Entities;

namespace Micro_ORM_Tasks.Tests
{
	[TestClass]
	public class CategoryControllerTests
	{
		private IDictionary<int, Category> categories;
		private CategoryService categoryService;

		[TestInitialize]
		public void TestInit()
		{
			categories = GetTestCategories(5);
			ICategoryRepo categoryRepo = new CategoryDictionary(categories);
			categoryService = new CategoryService(categoryRepo);
		}

		[TestMethod]
		public void GetAllCategories_ShouldReturnAllCategories()
		{
			// arrange
			CategoryController categoryController = new CategoryController(categoryService);

			// act 
			IEnumerable<Category> actualCategories = categoryController.GetCategories();

			// assert
			Assert.AreEqual(categories.Count, actualCategories.ToList().Count);
		}

		[TestMethod]
		public void GetCategory_ShouldReturnSpecifiedCategory()
		{
			// arrange
			CategoryController categoryController = new CategoryController(categoryService);
			int categoryId = 3;

			// act
			Category actualCategory = categoryController.GetCategory(categoryId);

			// assert
			Assert.AreSame(categories[categoryId], actualCategory);
		}

		[TestCleanup]
		public void TestClean()
		{
			categories = null;
			categoryService = null;
		}

		#region Prerequisites

		private Random randomizer = new Random();

		private IDictionary<int, Category> GetTestCategories(int count)
		{
			return Enumerable
				.Range(1, count)
				.Select(iterator => new KeyValuePair<int, Category>(
					iterator,
					new Category
					{
						CategoryID = iterator,
						CategoryName = GetRandomString(4),
						Description = GetRandomString(4)
					}))
				.ToDictionary(entity => entity.Key, entity => entity.Value);
		}

		private string GetRandomString(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string(Enumerable
				.Repeat(chars, length)
				.Select(str => str[randomizer.Next(str.Length)])
				.ToArray());
		}

		#endregion
	}
}