﻿using System.Collections.Generic;
using Micro_ORM_Tasks.Domain.Entities;

namespace Micro_ORM_Tasks.DAL.Interfaces
{
	public interface IOrderRepo
	{
		IEnumerable<Order> GetAllOrders();
		Order GetOrderById(int id);
		bool InsertOrder(Order order);
		bool UpdateOrder(Order order);
		bool DeleteOrderById(int id);
	}
}