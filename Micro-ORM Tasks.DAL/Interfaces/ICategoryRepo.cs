﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Micro_ORM_Tasks.Domain.Entities;

namespace Micro_ORM_Tasks.DAL.Interfaces
{
	public interface ICategoryRepo
	{
		IEnumerable<Category> GetAllCategories();
		Category GetCategoryById(int id);
		bool InsertCategory(Category category);
		bool UpdateCategory(Category category);
		bool DeleteCategoryById(int id);
	}
}
