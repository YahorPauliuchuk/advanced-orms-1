﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using Micro_ORM_Tasks.DAL.Interfaces;
using Micro_ORM_Tasks.Domain.Entities;

namespace Micro_ORM_Tasks.DAL.Components
{
	public class OrderRepo : IOrderRepo
	{
		private IDbConnection GetOpenConnection()
		{
			IDbConnection connection = new SqlConnection
			{
				ConnectionString = ConfigurationManager
					.ConnectionStrings["NorthwindDB"]
					.ConnectionString
			};

			connection.Open();
			return connection;
		}

		public IEnumerable<Order> GetAllOrders()
		{
			return GetOrdersMap();
		}

		//TODO:
		public Order GetOrderById(int id)
		{
			throw new NotImplementedException();
		}

		//TODO:
		public bool InsertOrder(Order order)
		{
			throw new NotImplementedException();
		}

		//TODO:
		public bool UpdateOrder(Order order)
		{
			throw new NotImplementedException();
		}

		//TODO:
		public bool DeleteOrderById(int id)
		{
			throw new NotImplementedException();
		}

		private IEnumerable<Order> GetOrdersMap()
		{
			string sql = "SELECT * " +
				"FROM [Orders] " +
				"INNER JOIN [OrderDetails] ON [Orders].OrderID = [OrderDetails].OrderID";

			using (IDbConnection connection = GetOpenConnection())
			{
				IDictionary<int, Order> orderDictionary = new Dictionary<int, Order>();

				IEnumerable<Order> orders = connection.Query<Order, OrderDetail, Order>(
					sql,
					map: (order, orderDetail) =>
					{
						if (!orderDictionary.TryGetValue(order.OrderID, out Order orderEntry))
						{
							orderEntry = order;
							orderEntry.OrderDetails = new List<OrderDetail>();
							orderDictionary.Add(orderEntry.OrderID, orderEntry);
						}
						orderEntry.OrderDetails.Add(orderDetail);
						return orderEntry;
					},
					splitOn: "OrderID"
				).Distinct();
				return orders;
			}
		}
	}
}