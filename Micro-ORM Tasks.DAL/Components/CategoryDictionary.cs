﻿using System.Collections.Generic;
using Micro_ORM_Tasks.Domain.Entities;
using Micro_ORM_Tasks.DAL.Interfaces;

namespace Micro_ORM_Tasks.DAL.Components
{
	public class CategoryDictionary : ICategoryRepo
	{
		private IDictionary<int, Category> categoryTable;

		public CategoryDictionary(IDictionary<int, Category> data)
		{
			categoryTable = data;
		}

		public IEnumerable<Category> GetAllCategories()
		{
			return categoryTable.Values;
		}
		
		public Category GetCategoryById(int id)
		{
			return categoryTable[id];
		}
		
		public bool InsertCategory(Category category)
		{
			if (!categoryTable.ContainsKey(category.CategoryID))
			{
				categoryTable.Add(category.CategoryID, category);
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool UpdateCategory(Category category)
		{
			if (!categoryTable.ContainsKey(category.CategoryID))
			{
				categoryTable[category.CategoryID] = category;
				return true;
			}
			else
			{
				return false;
			}
		}
		// TODO:
		public bool DeleteCategoryById(int id)
		{
			throw new System.NotImplementedException();
		}
	}
}