﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using Micro_ORM_Tasks.DAL.Interfaces;
using Micro_ORM_Tasks.Domain.Entities;

namespace Micro_ORM_Tasks.DAL.Components
{
	public class CategoryRepo : ICategoryRepo
	{
		private IDbConnection GetOpenConnection()
		{
			IDbConnection connection = new SqlConnection
			{
				ConnectionString = ConfigurationManager
					.ConnectionStrings["NorthwindDB"]
					.ConnectionString
			};

			connection.Open();
			return connection;
		}

		public IEnumerable<Category> GetAllCategories()
		{
			return GetCategoriesMapDouble();
		}

		public Category GetCategoryById(int categoryId)
		{
			string sql = "SELECT * FROM [Categories] " +
				"WHERE CategoryID = @CategoryID";

			using (IDbConnection connection = GetOpenConnection())
			{
				return connection.Query<Category>(
					sql,
					new { CategoryID = categoryId }
				).SingleOrDefault();
			}
		}
		// TODO:
		public bool InsertCategory(Category category)
		{
			throw new NotImplementedException();
		}
		// TODO:
		public bool UpdateCategory(Category category)
		{
			throw new NotImplementedException();
		}
		// TODO:
		public bool DeleteCategoryById(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Category> GetCategoriesMap()
		{
			string sql = "SELECT * " +
				"FROM [Categories]" +
				"INNER JOIN [Products] ON [Categories].CategoryID = [Products].CategoryID";

			using (IDbConnection connection = GetOpenConnection())
			{
				IDictionary<int, Category> categoryDictionary = new Dictionary<int, Category>();

				IEnumerable<Category> categories = connection.Query<Category, Product, Category>(
					sql,
					map: (category, product) =>
					{
						if (!categoryDictionary.TryGetValue(category.CategoryID, out Category categoryEntry))
						{
							categoryEntry = category;
							categoryEntry.Products = new List<Product>();
							categoryDictionary.Add(categoryEntry.CategoryID, categoryEntry);
						}
						categoryEntry.Products.Add(product);
						return categoryEntry;
					},
					splitOn: "CategoryID"
				).Distinct();
				return categories;
			}
		}

		public IEnumerable<Category> GetCategoriesMapDouble()
		{
			string sql = "SELECT * " +
				"FROM [Categories] " +
				"INNER JOIN [Products] ON [Categories].CategoryID = [Products].CategoryID " +
				"INNER JOIN [OrderDetails] ON [Products].ProductID = [OrderDetails].ProductID";
			
			using (IDbConnection connection = GetOpenConnection())
			{
				IDictionary<int, Category> categoryDictionary = new Dictionary<int, Category>();

				IEnumerable<Category> categories = connection.Query<Category, Product, OrderDetail, Category>(
					sql,
					map: (category, product, orderDetail) =>
					{
						if (!categoryDictionary.TryGetValue(category.CategoryID, out Category categoryEntry))
						{
							categoryEntry = category;
							categoryEntry.Products = new List<Product>();
							categoryEntry.Products.Select(productEntry => productEntry.OrderDetails = new List<OrderDetail>());

							categoryDictionary.Add(categoryEntry.CategoryID, categoryEntry);
						}
						categoryEntry.Products.Add(product);
						foreach (Product productEntry in categoryEntry.Products)
						{
							productEntry.OrderDetails.Add(orderDetail);
						}

						return categoryEntry;
					},
					splitOn: "CategoryID, ProductID"
				).Distinct();
				return categories;
			}
		}
	}
}